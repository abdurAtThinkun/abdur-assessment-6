﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Java.Lang;
using System;
using Android.Graphics;

namespace AsyncImageLoading
{
    [Activity(Label = "AsyncImageLoading", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity, LoaderManager.ILoaderCallbacks
    {
        
        Button downloadButton;
        ImageView imageView;
        ProgressDialog progressDialog;
        string URL = "https://goo.gl/pS5FD4";
        Bitmap bitmapImage;

       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            downloadButton = FindViewById<Button>(Resource.Id.downloadButton);
            imageView = FindViewById<ImageView>(Resource.Id.imageView);

            downloadButton.Click += delegate { OnClickingDownloadButton(); };

        }

       
        protected override void OnSaveInstanceState( Bundle outState)
        {
            
            outState.PutParcelable("BitmapImage", bitmapImage);
            base.OnSaveInstanceState(outState);  
           
        }

       
        protected override void OnRestoreInstanceState( Bundle savedInstanceState)
        {
            
            imageView.SetImageBitmap(savedInstanceState.GetParcelable("BitmapImage") as Bitmap);
            base.OnSaveInstanceState(savedInstanceState);

        }

       
        private void OnClickingDownloadButton()
        {
            LoaderManager.InitLoader(0, null, this);
        }
     


        public Loader OnCreateLoader(int id, Bundle args)
        {
            progressDialog = new ProgressDialog(this);
            progressDialog.SetTitle("Downloading Image");
            progressDialog.SetMessage("Loading....");
            progressDialog.SetCancelable(false);
            progressDialog.Show();

            return new ImageDownloaderActivity(this, URL);
        }

       
        public void OnLoaderReset(Loader loader)
        {
            imageView.SetImageBitmap(null);
        }


        public void OnLoadFinished(Loader loader, Java.Lang.Object data)
        {
            var downloadedImage = data as Bitmap;

            bitmapImage = downloadedImage;

            imageView.SetImageBitmap(downloadedImage);

            progressDialog.Dismiss();
        }
    }


}

