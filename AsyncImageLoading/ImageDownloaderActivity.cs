﻿using System;
using System.Net;
using Android.Content;
using Android.Graphics;
using Java.Lang;

namespace AsyncImageLoading
{
    public class ImageDownloaderActivity : AsyncTaskLoader
    {
        private Context context;
        private string URL;


        public ImageDownloaderActivity(Context context, string URL) : base(context)
        {
            this.context = context;
            this.URL = URL;
        }

        protected override void OnStartLoading()
        {
            ForceLoad();
        }

        public override Java.Lang.Object LoadInBackground()
        {
            Bitmap bitmap = null;
            using (var webClient = new WebClient())
            {
                var bytes = webClient.DownloadData(URL);
                if (bytes != null && bytes.Length > 0)
                {
                    bitmap = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);

                }

             return bitmap;
            }
        }

        public override void DeliverResult(Java.Lang.Object bitmap)
        {
            base.DeliverResult(bitmap);
        }
     


    }
}
